
name = "Jose"
age = 35
occupation = "singer"
movie = "West Side"
rating = 99.6

print(f"I am {name}, I am {age} years old. I am a {occupation}. My favorite movie is {movie} and my rating is {rating}")

num1, num2, num3 = 10, 20, 30

num3 = num1 * num2
print(num3)

num2 = num1 > num3
print(num2)

num1 = num2 + num3
print(num1)