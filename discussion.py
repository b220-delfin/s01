from pickle import TRUE


print("Hello World")

# print is similar to console.log
# comment
""" for multi line comments in]
python 
comment
"""

""" Python Basics
Variable - we dont have to use keywords in python to set variable
"""

age = 35
middle_initial = 'C'

print(age)
print(middle_initial)

# in python, we use snake case, convention in Python in terms in naming variables in Python, uses _ underscore between words and uses lowercasing

name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"
print(name1, name2, name3, name4)


# Data Types
full_name = "John Doe" #string
# number
num_of_days = 365 #integer
pi_approx = 3.1416 # float
complex_num = 1+5j # complex

#boolean
is_learning = True
is_difficult = False

print("Hi! My name is " + full_name)

# [Section] f strings
# similar to JS template literals
print(f"Hi! My name is {full_name} and my age is {age}.")
# Typecasting
# is Python's way of converting one data type to another
# print("My age is " + age) --error

#from int to string
print("My age is " + str(age))

# from string to in
print(age + int("9876"))


# Operators
#addition, sub, multiply, divide, modulo, exponent

#Assignment Operators
num1 = 3
print(num1)
num1 = num1 + 4
print(num1)

num1 = 3
print(num1)
num1 += 4
print(num1)
num1 -= 4
print(num1)
num1 *= 4
print(num1)
num1 /= 4
print(num1)
num1 %= 4
print(num1)

#Comparison Operators
# returns boolean value to compare values

print(1 == 1)
print(1 != 1)
print(1 < 1)
print(1 > 1)
print(1 <= 1)
print(1 >= 1)

# Logical Operators

print(True and False)
print(True or False)
print(not False)
